class User < ActiveRecord::Base
  has_secure_password
  validates_presence_of :password, :on => :create
  validates :username, uniqueness: true
  has_many :lists, dependent: :destroy
  has_many :auth_tokens, dependent: :destroy
end
