class List < ActiveRecord::Base
	has_many :items, dependent: :destroy
	validates :title, presence: true, length: {minimum: 3, maximum: 50}
	validates :user_id, presence: true
	belongs_to :user
end
