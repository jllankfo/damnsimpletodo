require 'securerandom'

class AuthToken < ActiveRecord::Base
  belongs_to :user
  validates :user_id, presence: true
  validates :token, uniqueness: true
  before_create :generate_token

  def generate_token
  	begin
  		self.token = SecureRandom.hex
  	end while self.class.exists?(token: token)
  end
end
