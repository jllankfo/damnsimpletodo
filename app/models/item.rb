class Item < ActiveRecord::Base
  belongs_to :list
  after_initialize :init

  def init
  	self.done = false if self.done.nil?
  end
end
