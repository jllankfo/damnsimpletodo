# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
 success = (data,textStatus,xhr) ->
 	location = xhr.getResponseHeader('Location')
 	window.location.replace(location)

 parseList = (text) ->
 	list = new Object();
 	items = text.val().split("\n")
 	list.items = Array();
 	i=0
 	for l in items
 		list.items.push({name : items[i]})
 		i++
 	return list


 $('#save').bind 'click', (event) ->
 	list = parseList($('#list'))
 	list.title = $('#title').val()
 	$.ajax({
 		type: "POST",
 		url: "/list",
 		data: JSON.stringify(list),
 		success: success,
 		contentType: "application/json; charset=utf-8",
 		dataType: "json",
 	});

 $('#add').bind 'click', (event) ->
 	list = parseList($('#list'))
 	id = $('span#data').data('id')
 	console.log(id)
 	if (id)
 		console.log("request")
 		$.ajax({
			type: "PUT",
			url: "/list/"+id,
			data: JSON.stringify(list),
			success: success,
			contentType: "application/json; charset=utf-8",
			dataType: "json",
 		});
