$ ->

 $('.list-item').bind 'click', (event) ->
 	item = new Object()
 	$(this).toggleClass('checked')
 	if $(this).hasClass('checked')
 		item.done=true
 	else
 		item.done = false
 	id = $(this).data('id')
 	if (id)
	 	$.ajax({
	 		type: "PUT",
	 		url: "/items/"+id,
	 		data: JSON.stringify(item),
	 		success: (->),
	 		contentType: "application/json; charset=utf-8",
	 		dataType: "json",
	 	});
