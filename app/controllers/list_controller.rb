class ListController < ApplicationController
	respond_to :json
	#TODO: compute recent feeds once and cache
	before_action :authenticate
	before_action :recent_lists, only: [:edit,:show,:new,:index]

	def recent_lists
		@recent_lists = @user.lists.order("created_at DESC").limit(10)
	end

	def create
		@list = @user.lists.new({:title => params[:title]})
		@list.items.new(params[:items]) if params[:items]
		if @list.save
			respond_to do |format|
				format.html
				format.json{render :json => @list, :status => :created, :location => @list}
			end
		end
	end

	def show
		#TODO BADDDD FIX...
		@lists = @user.lists.order('created_at DESC')
		@list = @user.lists.find(params[:id])
		@breadcrumbs = ['Lists', @list.title]

		respond_to do |format|
			format.html
			format.json {render json: @list.to_json(:include => :items) }
		end
	end

	def new

	end

	def index
		@lists = @user.lists.order('created_at DESC')
		listJson = {:lists => @lists}
		respond_to do |format|
			format.html
			format.json  {render json: listJson}
		end
	end

	def destroy

		respond_to do |format|
			@list = @user.lists.find(params[:id])
			if @list.destroy
				format.json {render nothing: true}
				format.html {redirect_to("/list")}
			else
				format.html {redirect_to("/list")}
				format.json {render json: @list.errors}
			end
		end
	end

	def update
		@list = @user.lists.find(params[:id])

		items = @list.items.create(params[:items])
		if items
			respond_to do |format|
				format.html
				format.json{render :json => {:items => items}, :status => :ok, :location => @list}
			end
		end
	end

	def edit
		@list = @user.lists.find(params[:id])
		@breadcrumbs = [@list.title, 'Edit']
	end

	def checked
		@list = @user.lists.find(params[:list_id])
		Item.where(list_id: params[:list_id], done: true).delete_all
		respond_to do |format|
			format.html {redirect_to @list}
			format.json {render nothing: true}
		end
	end
end
