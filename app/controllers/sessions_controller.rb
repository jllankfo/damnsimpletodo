class SessionsController < ApplicationController
  def new
  end

  def create
  	user = User.find_by_username(params[:username])
  	if user && user.authenticate(params[:password])
  		respond_to do |format|

  			format.json do
  				token = user.auth_tokens.create
  				render json: token
  			end

  			format.html do
		  		session[:user_id] = user.id
		  		redirect_to root_url
		  	end
	  	end
  	else

      respond_to do |format|
  		  format.html do
          flash.now.alert = "Invalid username or password. Please try again."
  		    render 'new'
        end

        format.json do
          render json: {"error" => "Invalid username or password. Please try again."}
        end
      end
  	end
  end

  def destroy
  		respond_to do |format|
  			format.html do
			  session[:user_id] = nil
			  redirect_to root_url
			end

			format.json do
				authenticate_or_request_with_http_token do |token, options|
					token = AuthToken.find_by_token(token)
					if token
						token.destroy
						head :ok
					end
				end
			end
		end
  end

end
