class PagesController  < ApplicationController
	before_action :recent_lists

	def about
		@breadcrumbs = ['About']
	end

	def contact
		@breadcrumbs = ['Contact']
	end

	def recent_lists
		if session[:user_id]
			@user = User.find(session[:user_id])
			@recent_lists = @user.lists.order("created_at DESC").limit(10) if @user
		end
	end
end
