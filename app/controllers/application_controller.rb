class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #protect_from_forgery with: :exception

  private 
	def authenticate
		#Try a session first, then try an auth token
		if session[:user_id]
			@user = User.find(session[:user_id])
			return
		else
			 authenticate_with_http_token  do |token, options|
				auth_token = AuthToken.find_by_token(token)
				if auth_token 
					@user = auth_token.user
					return
				end
			end
		end

		#if it reaches this point we are not authenticated so respond approriately
		respond_to do |format|
			format.html {render 'pages/unauthorized'}
			format.json {head :unauthorized}
		end
		
	end
end
