class UsersController < ApplicationController
  def new
  	@user = User.new
  end

  def create
  	@user = User.new(params[:user].permit(:username,:password,:password_confirmation))
  	if @user.save
  		redirect_to root_url, :notice => "Registration successful"
  	else
      if @user.errors.any?
        flash.now.alert = @user.errors.full_messages[0]
      end
  		render "new"
  	end
  end
end
