class ItemsController < ApplicationController
	before_action :authenticate
	def update
		@item = Item.find(params[:id])

		#Validate that the user is updating an item in their own lists
		if @item.list.user != @user
			return head :unauthorized
		end

		if @item.update(params[:item].permit(:name,:done))
			respond_to do |format|
				format.html
				format.json {render :json => @item, :status => :ok}
			end
		end
	end
end
