module ListHelper

	def checked_list_path(list)
		return "/list/#{list.id}/checked"
	end
end
