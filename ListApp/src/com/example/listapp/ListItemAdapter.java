package com.example.listapp;

import java.util.List;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.listapp.models.Item;

public class ListItemAdapter<T> extends ArrayAdapter<Item> {

	Context context;
	List<Item> items;
	
	public ListItemAdapter(Context context, int resource, List<Item> objects) {
		super(context, resource, objects);
		this.context = context;
		this.items = objects;
	
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if (v == null) {
			LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = li.inflate(android.R.layout.simple_list_item_1, null);
		}
		
		TextView tv = (TextView) v.findViewById(android.R.id.text1);
		Item item = items.get(position);
		tv.setText(item.toString());
		if (item.isDone()){
			tv.setPaintFlags(tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
		}else{
			tv.setPaintFlags(tv.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
		}
		
		return tv;
	}
}
