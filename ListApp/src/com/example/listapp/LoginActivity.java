package com.example.listapp;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.example.listapp.services.LoginService;

public class LoginActivity extends Activity {

	LoginService loginService;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		setClickListeners();
		loginService = new LoginService(getBaseContext());
	}

	private void setClickListeners() {
		Button button = (Button) findViewById(R.id.login_submit);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				EditText user = (EditText) findViewById(R.id.username);
				EditText pass = (EditText) findViewById(R.id.password);
				
				try {
					loginService.login(user.getText().toString(), pass.getText().toString(), new AjaxCallback<JSONObject>(){
						public void callback(String url, JSONObject result, AjaxStatus status){
							if (status.getCode() != 200 || result == null) {
								Toast.makeText(getBaseContext(), "Invalid request", Toast.LENGTH_SHORT).show();
								return;
							}
							
							try{ 
								if (result.has("token")) { //Valid token, store it and start the main activity
									String token = result.getString("token");
									loginService.setAuthToken(token); //Stores token in memory and in preference share
									Toast.makeText(getBaseContext(), "Successfully logged in", Toast.LENGTH_SHORT).show();
									finish();
									return;
									
								}else{ //An error occured, try to get the exact error from the json response
									String error = "An error occured";
									if (result.has("error")) error = result.getString("error");
									Toast.makeText(getBaseContext(), error, Toast.LENGTH_SHORT).show();
								}
								
							}catch(Exception e){
								e.printStackTrace(); //This should never occur, means the response was corrupted
							}
						}
								
					});
				} catch (JSONException e) {
					Toast.makeText(getBaseContext(), "Error logging in", Toast.LENGTH_SHORT).show();
				}
			}
			
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

}
