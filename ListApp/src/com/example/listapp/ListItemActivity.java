package com.example.listapp;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.example.listapp.models.Item;
import com.example.listapp.models.TodoList;
import com.example.listapp.services.ItemService;
import com.example.listapp.services.ListService;
import com.example.listapp.utils.DSTConstants;
import com.example.listapp.utils.MagazineDialogBuilder;

public class ListItemActivity extends ListActivity {
	private List<Item> items = new ArrayList<Item>();
	private ListItemAdapter<Item> adapter = null;
	private ListService listService = new ListService(getBaseContext());
	private ItemService itemService = new ItemService(getBaseContext());
	
	private int mId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_layout);
		// Show the Up button in the action bar.
		setupActionBar();
		
		Intent intent = getIntent();
		mId = intent.getExtras().getInt(MainActivity.LIST_ID);
		adapter = new ListItemAdapter<Item>(this,android.R.layout.simple_list_item_1,items);
		
		setListAdapter(adapter);
		fetchList(mId);
		setTitle("");
		setupItemUpdates();
	}

	private void fetchList(int mId) {
		listService.fetchList(mId, new AjaxCallback<TodoList>(){
			public void callback(String url, TodoList list, AjaxStatus status) {
				if (list != null){ 
					items.clear();
					items.addAll(list.getItems());
					adapter.notifyDataSetChanged();
					setTitle(list.getTitle());
				}
			}
		});
		
	}

	private void setupItemUpdates() {
		getListView().setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> listView, View view, int pos,
					long id) {
				
				final TextView tv = (TextView) view.findViewById(android.R.id.text1);
				final Item item = items.get(pos);
				final boolean done = item.isDone();
				
				itemService.toggleItem(item.getId(),!done,(new AjaxCallback<JSONObject>() {
					public void callback(String url, JSONObject result, AjaxStatus status){
						if (result == null) return;
						if (done) //It was marked as done so undo strikethrough
							tv.setPaintFlags(tv.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
						else
							tv.setPaintFlags(tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
						//Change the done in the item list so the redraw is correct, only after the put goes through
						item.setDone(!done);
					}
				}));
				
				Log.i("CLICKED", "pos:" + pos + " id:" + id);
			}
			
		});
	}



	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.list_menu, menu);
	    
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		case R.id.add_button:
			buildAlert().show();
			break;
		case R.id.magazine_mode:
			//Launch goggles
			  SharedPreferences prefs = getSharedPreferences(DSTConstants.PREFS, 0);
			  boolean dontShow = prefs.getBoolean(DSTConstants.TUT0,false );
			  if (!dontShow) {
				 buildTutorialAlert().show();
			  }else{
			      launchGoggles();
			  }
			break;
		case R.id.clear:
			listService.clearChecked(mId, new AjaxCallback<String>(){
				public void callback(String url, String s,AjaxStatus status) {
					if (status.getCode() == 200)
						fetchList(mId);
				}
			});
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == R.id.magazine_mode) {
			
			//Get clipboard data
			ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
			ClipData clipData = clipboard.getPrimaryClip();
			if (clipData == null || clipData.getItemCount() <= 0) return;
			ClipData.Item clipItems = clipData.getItemAt(0);
			ArrayList<String> itemsList = new ArrayList<String>();
			
			//Copy items to list
			if (clipItems != null){
				String text = clipItems.coerceToText(this).toString();
				for (String item : text.split(System.getProperty("line.separator"))){
					if (item != null && !item.equals("")) {
						itemsList.add(item);
					}
			}
				
			if (itemsList.size() == 0) return;
				
			String[] newItems = new String[itemsList.size()];
			itemsList.toArray(newItems);
			new MagazineDialogBuilder(this,newItems,mId).show();
		}
	}
	}

	public void updateList(int id, String[] newItems) {
		try {
			listService.addItems(newItems, mId, new AjaxCallback<TodoList>(){
				public void callback(String url, TodoList addedList, AjaxStatus status) {
					//Only returns a list of the added items (more efficient)
					if (addedList != null){ 
						items.addAll(addedList.getItems());
						adapter.notifyDataSetChanged();
					}
				}
			});
		}catch (UnsupportedEncodingException e)
		{
			//TODO
			e.printStackTrace();
		}
		
	}
	

	private void launchGoggles() {
		//Launch goggles
	    Intent intent1 = new Intent("com.google.android.apps.unveil.ACTION_TAKE_PHOTO");
	    intent1.setPackage("com.google.android.apps.unveil");
	    startActivityForResult(intent1, R.id.magazine_mode);
	}
	
	//Create alert for adding new items to the list
	private AlertDialog.Builder buildAlert() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Create Item");
		alert.setMessage("New Items:");
		
		// Set an EditText view to get user input 
		final EditText input = new EditText(this);
		input.setHint("Separate with comma");
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int whichButton) {
			
				String text = input.getText().toString();
				if (text == null || text.equals("")) return;
				String newItems[] = text.split(",");
				
				updateList(mId,newItems);
		  }
		});

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		  public void onClick(DialogInterface dialog, int whichButton) {
		    // Canceled.
		  }
		});
		
		return alert;
	}

	
	//Create alert for adding new items to the list
	private AlertDialog.Builder buildTutorialAlert() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("How to use Magazine Mode");
		alert.setMessage("1. Take a clear photo of ingredients\n" +
	      		   "2. Open the text and copy to clipboard\n" +
	      		   "3. Hit back a few times until this app resumes\n" +
	      		   "4. Make any necessary edits, fixing lines etc");

		final CheckBox box = new CheckBox(this);
		box.setText("Don't show this again.");
		alert.setView(box);
		
		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				//Store prefs
				if (box.isChecked()) {
					SharedPreferences prefs = ListItemActivity.this.getSharedPreferences(DSTConstants.PREFS, 0);
					Editor edit = prefs.edit();
					edit.putBoolean(DSTConstants.TUT0, true);
					edit.commit();
				}
				
				launchGoggles();
			}
			});
		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		  public void onClick(DialogInterface dialog, int whichButton) {
		    // Canceled.
		  }
		});
		
		return alert;
	}

}
