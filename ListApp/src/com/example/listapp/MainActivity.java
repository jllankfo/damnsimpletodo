package com.example.listapp;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.example.listapp.models.TodoList;
import com.example.listapp.services.ListService;
import com.example.listapp.services.LoginService;

public class MainActivity extends ListActivity {
	
	private List<TodoList> lists = new ArrayList<TodoList>();
	private ArrayAdapter<TodoList> adapter;
	private ListService listService;
	private LoginService loginService;
	
	public static final String LIST_ID = "com.example.listapp.LIST_ID";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		listService = new ListService(getBaseContext());
		loginService = new LoginService(getBaseContext());
		//Do we need to login?
		
		if (loginService.getAuthToken() == null) //Auth token is shared between all instance of the services
		{
			Intent intent = new Intent(this,LoginActivity.class);
			startActivityForResult(intent,0);
			
		}else{
			queryLists();
		}
		
		setContentView(R.layout.list_layout);
		
		adapter = new ArrayAdapter<TodoList>(this,android.R.layout.simple_list_item_1,lists);
		setListAdapter(adapter);
		setupListClick();
		registerForContextMenu(getListView());
		setTitle("Lists");
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		queryLists();
	}

	private void setupListClick() {
		getListView().setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> listView, View view, int pos,long id) {
				
				TodoList list = lists.get(pos);
				if (list == null) return;
				int lid = list.getId();
				Intent intent = new Intent(MainActivity.this,ListItemActivity.class);
				intent.putExtra(LIST_ID, lid);
				startActivity(intent);
			}
		});
	}

	//TODO: FIX
	private AlertDialog.Builder buildAlert() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Find List");
		alert.setMessage("List ID:");

		// Set an EditText view to get user input 
		final EditText input = new EditText(this);
		input.setInputType(InputType.TYPE_CLASS_NUMBER);
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int whichButton) {

			Intent intent = new Intent(MainActivity.this,ListItemActivity.class);
			if (input.getText() == null || input.getText().equals("")) return;
			int id = Integer.parseInt(input.getText().toString());
			intent.putExtra(LIST_ID, id);
			startActivity(intent);
		  }
		});

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		  public void onClick(DialogInterface dialog, int whichButton) {
		    // Canceled.
		  }
		});
		
		return alert;
	}

	//TODO: FIX
	//Creates an empty list
	private AlertDialog.Builder buildCreateNew() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Create New List");
		alert.setMessage("Title:");

		// Set an EditText view to get user input 
		final EditText input = new EditText(this);
		input.setInputType(InputType.TYPE_CLASS_TEXT);
		alert.setView(input);
		
		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int whichButton) {
			listService.createList(input.getText().toString(),new AjaxCallback<TodoList>(){
				public void callback (String url, TodoList list, AjaxStatus status)
				{
					if (list == null) return;
					//Update lists
					lists.add(list);
					adapter.notifyDataSetChanged();
					
					//Start an activity to add items to list
					int id = list.getId();
					Intent intent = new Intent(MainActivity.this,ListItemActivity.class);
					intent.putExtra(LIST_ID, id);
					startActivity(intent);
				}
			});
		}
		});
		
		
		return alert;
	}
	
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		Log.i("MENU_ITEM", item.getItemId()+"");
		switch(item.getItemId()){
		case R.id.find_list:
			buildAlert().show();
			break;
		case R.id.refresh:
			queryLists();
			break;
		case R.id.create_new:
			buildCreateNew().show();
			break;
		case R.id.logout:
			loginService.logout(); //This will delete the auth token on the server and locally
			Intent intent = new Intent(MainActivity.this,LoginActivity.class);
			startActivityForResult(intent,0);
			break;
		}
		return super.onMenuItemSelected(featureId, item);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		switch(item.getItemId()) {
		case R.id.delete:
			delete(info.id);
			break;
		}
		return super.onContextItemSelected(item);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		getMenuInflater().inflate(R.menu.context_main, menu);
		super.onCreateContextMenu(menu, v, menuInfo);
	}
	
	//LIST OPS//
	
	private void delete(final long listPos) {
		
		TodoList list = lists.get((int)listPos);
		if (list == null) return;
		int id = list.getId();
		
		listService.deleteList(id, new AjaxCallback<String>(){
			public void callback(String url, String s,AjaxStatus status) {
				
				if (status.getCode() == 200) {
					lists.remove((int)listPos);
					adapter.notifyDataSetChanged();
				}
			}
		});
	}
	

	private void queryLists() {
		listService.fetchAll(new AjaxCallback<JSONObject>(){
			public void callback (String url, JSONObject json, AjaxStatus status)
			{
				//If the auth token is invalid, start the login activity
				if (status.getCode() == HttpStatus.SC_UNAUTHORIZED) {
					Intent intent = new Intent(MainActivity.this,LoginActivity.class);
					startActivityForResult(intent,0);
				}
				
				if (json == null) return;
					
				try {
					JSONArray arr = json.getJSONArray("lists");
					lists.clear();
					for (int a=0; a<arr.length(); a++){
						JSONObject obj = arr.getJSONObject(a);
						TodoList list = new TodoList(obj.getString("title"),obj.getInt("id"));
						lists.add(list);
					}
					adapter.notifyDataSetChanged();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			
			}
		});
		
	}

}
