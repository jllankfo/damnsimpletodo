package com.example.listapp.tasks;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import android.graphics.Paint;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import com.example.listapp.models.Item;


public class UpdateItemTask extends AsyncTask<Long, Void, Void> {
	TextView view;
	boolean done;
	List<Item> items;
	public UpdateItemTask(TextView view,List<Item> items) {
		this.view = view;
		this.items = items;
	}
	@Override
	protected Void doInBackground(Long... args) {
		long id = args[0];
		
		view.findViewById(android.R.id.text1);
		if ((view.getPaintFlags() & Paint.STRIKE_THRU_TEXT_FLAG) > 0){
			done = true;
		}else {
			done = false;
		}
		
		HttpClient httpClient = new DefaultHttpClient();
		HttpPut request = new HttpPut("http://192.168.1.129:3000/items/"+items.get((int) id).getId());
		request.addHeader("Content-Type","application/json");
		request.addHeader("Accept", "application/json");
		ResponseHandler<String> handler = new BasicResponseHandler();
		String result = null;
		try{
			request.setEntity(new StringEntity(buildJson(!done)));
			result = httpClient.execute(request,handler);
			
		} catch (ClientProtocolException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		
		httpClient.getConnectionManager().shutdown();
		if (result!= null) Log.i("WEBCALL", result);
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if (done)
			view.setPaintFlags(view.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
		else
			view.setPaintFlags(view.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
	}
	private String buildJson(boolean b) {
		return "{ \"item\": { \"done\":\"" + b + "\"}}";
	}

	
}
