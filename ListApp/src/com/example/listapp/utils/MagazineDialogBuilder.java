package com.example.listapp.utils;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.example.listapp.ListItemActivity;
import com.example.listapp.R;

public class MagazineDialogBuilder extends Builder {

	private ListItemActivity activity;
	private int id;
	private final String LS = System.getProperty("line.separator");
	
	public MagazineDialogBuilder(Context context, String[] items, int id) {
		super(context);
		initializeDialog(items);
		activity = (ListItemActivity) context;
		this.id = id;
	}

	
	public void initializeDialog(String[] items) {
		
        setTitle("Magazine Fixer");

        LayoutInflater li = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view =  li.inflate(R.layout.magazine_view, null);
		final EditText text = (EditText) view.findViewById(R.id.magazine_item);
		String allItems = "";
		for(String item : items)
			allItems += item + LS;
		text.setText(allItems);
		text.setHorizontallyScrolling(true);
        setView(view);
        
        setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    	String[] newItems = text.getText().toString().split(LS);
                        activity.updateList(id, newItems);
                    }
                });
        
       
        setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        
	}
}
