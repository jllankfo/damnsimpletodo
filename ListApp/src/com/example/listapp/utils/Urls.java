package com.example.listapp.utils;

public enum Urls {

	//BASE_URL("http://damnsimplelist.tk"),
	//BASE_URL("http://174.97.195.70"),
	//BASE_URL("http://192.168.1.129:3000"),
	BASE_URL("https://damnsimpletodo.herokuapp.com"),
	BASE_LIST(BASE_URL.url + "/list"),
	LIST_ID(BASE_LIST.url + "/{id}"),
	BASE_ITEMS(BASE_URL.url + "/items"),
	ITEMS(BASE_ITEMS.url + "/{id}"),
	CHECKED(LIST_ID.url + "/checked"), //resource that contains all 'done' items in list
	LOGIN(BASE_URL.url + "/log_in"),
	LOGOUT(BASE_URL.url + "/log_out");
	
	private String url;
	private final String ID = "{id}";
	
	Urls(String url) {
		this.url = url;
	}
	
	public String getUrl(int arg)
	{
		return getUrl(arg,false);
	}
	
	public String getUrl() {
		return url;
	}
	
	public String getUrl(int id, boolean appendJson)
	{
		String ret = url.replace(ID, id+"");
		if (appendJson) ret+= ".json";
		return ret;
	}
	
}
