package com.example.listapp.models;

import com.google.gson.annotations.Expose;


public class Item {
	
	@Expose private String name;
	private int id;
	
	//Fields not to include on create/update
	@Expose private boolean done = false;
	
	
	public Item(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public String toString() {
		return name;
	}
	
	public int getId() {
		return id;
	}
	
	public boolean isDone() {
		return done;
	}
	
	public void setDone(boolean done) {
		this.done = done;
	}
}

