package com.example.listapp.models;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

public class TodoList {

	@Expose private List<Item> items = new ArrayList<Item>();
	@Expose private String title;
	
	//Fields not to include on create/update
	private int id;
	
	public TodoList()
	{

	}
	
	public TodoList(String title, int id) {
		this.title =title;
		this.id = id;
	}
	
	public String toString() {
		
		return title;
	}

	public List<Item> getItems() {
		return items;
	}
	
	public int getId() {
		return id;
	}
	
	public String getTitle() {
		return title;
	}
}
