package com.example.listapp.services;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.entity.StringEntity;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.example.listapp.models.Item;
import com.example.listapp.models.TodoList;
import com.example.listapp.utils.GsonTransformer;
import com.example.listapp.utils.Urls;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ListService extends BaseService {

	//Any request that needs to be authenticated should use the getHeaders() to add the auth token
	public ListService(Context context) {
		super(context);
	}
	
	public void fetchList(int a, AjaxCallback<TodoList> callback) {
		AQuery aq = new AQuery(context);
		aq.transformer(new GsonTransformer()).ajax(Urls.LIST_ID.getUrl(a,true),TodoList.class,callback.headers(getHeaders()));
	}

	public void clearChecked(int mId, AjaxCallback<String> callback) {
		AQuery aq = new AQuery(context);
		aq.delete(Urls.CHECKED.getUrl(mId, true), String.class,callback.headers(getHeaders()));
		
	}
	
	public void addItems(String[] items, int id, AjaxCallback<TodoList> callback) throws UnsupportedEncodingException {
		AQuery aq = new AQuery(context);
		TodoList list = new TodoList(null,id);
		List<Item> itemList = list.getItems();
		for (String name : items)
			itemList.add(new Item(0,name.trim()));
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		Log.i("GSON",(gson.toJson(list)));
		StringEntity entity = new StringEntity(gson.toJson(list));
		
		aq.transformer(new GsonTransformer()).
		 put(Urls.LIST_ID.getUrl(id,true),"application/json", entity,TodoList.class,callback.headers(getHeaders()));
	}
	
	public void createList(String name, AjaxCallback<TodoList> callback) {
		JSONObject obj = new JSONObject();
		try{
			obj.put("title", name);
		}catch(Exception e) {
			
		}
		AQuery aq = new AQuery(context);
		aq.transformer(new GsonTransformer()).
			post(Urls.BASE_LIST.getUrl(), obj, TodoList.class,callback.headers(getHeaders()));
	}

	public void fetchAll(AjaxCallback<JSONObject> callback) {
		AQuery aq = new AQuery(context);
		aq.ajax(Urls.BASE_LIST.getUrl(), JSONObject.class,callback.headers(getHeaders()));
		
	}

	public void deleteList(int id, AjaxCallback<String> callback) {
		AQuery aq =  new AQuery(context);
		aq.delete(Urls.LIST_ID.getUrl(id), String.class, callback.headers(getHeaders()));
		
	}
}
