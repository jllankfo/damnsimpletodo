package com.example.listapp.services;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;

public class BaseService {
	protected Context context;
	public static String AUTH_TOKEN;
	
	public BaseService(Context context) {
		this.context = context;
		if (AUTH_TOKEN == null && context != null) {
			SharedPreferences prefs = context.getSharedPreferences("dst_prefs", 0);
			AUTH_TOKEN = prefs.getString("auth_token", null);
		}
		
	}
	
	public Map<String,String> getHeaders() {
		HashMap<String,String> map = new HashMap<String, String>();
		map.put("Accept", "application/json");
		map.put("Authorization", "Token token=\"" + AUTH_TOKEN + "\"");
		map.put("Content-Type", "application/json");
		
		return map;
	}
}
