package com.example.listapp.services;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.example.listapp.utils.DSTConstants;
import com.example.listapp.utils.Urls;

public class LoginService extends BaseService {
	
	public LoginService(Context context) {
		super(context);
	}

	public void login(String user, String pass, AjaxCallback<JSONObject> callback) throws JSONException
	{
		JSONObject json = new JSONObject();
		json.put("username", user);
		json.put("password", pass);
		
		AQuery aq = new AQuery(context);
		aq.post(Urls.LOGIN.getUrl(), json, JSONObject.class, callback.header("Accept", "application/json"));
	}
	
	public void logout()
	{
		AQuery aq = new AQuery(context);
		aq.ajax(Urls.LOGOUT.getUrl(),JSONObject.class,new AjaxCallback<JSONObject>(){}.headers(getHeaders()));
		setAuthToken(null);
	}

	public Object getAuthToken() {
		return AUTH_TOKEN;
	}

	public void setAuthToken(String token) {
		AUTH_TOKEN = token;
		SharedPreferences prefs = context.getSharedPreferences(DSTConstants.PREFS, 0);
		Editor editor = prefs.edit();
		editor.putString("auth_token", token);
		editor.commit();
		
	}
}
