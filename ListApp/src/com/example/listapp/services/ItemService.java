package com.example.listapp.services;

import java.io.UnsupportedEncodingException;

import org.apache.http.entity.StringEntity;
import org.json.JSONObject;

import android.content.Context;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.example.listapp.utils.Urls;

public class ItemService extends BaseService {

	public ItemService(Context context)
	{
		super(context);
	}
	
	/**
	 * Asynchronously toggle an item, and reutrn a json object with the response
	 */
	public void toggleItem(int id, boolean done, AjaxCallback<JSONObject> callback) {
	
		String url = Urls.ITEMS.getUrl(id,true);
		
		AQuery aq = new AQuery(context);
		StringEntity entity = null;
		
		try {
			entity = new StringEntity("{ \"item\": { \"done\":\"" + done + "\"}}");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		if (entity == null) return;
		
		aq.put(url, "application/json", entity, JSONObject.class, callback.headers(getHeaders()));
	}
}
